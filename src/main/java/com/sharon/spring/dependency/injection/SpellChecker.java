package com.sharon.spring.dependency.injection;

/**
 * Created by orensharon on 2/27/17.
 */
public class SpellChecker {
    public SpellChecker(){
        System.out.println("Inside SpellChecker constructor." );
    }

    public void checkSpelling(){
        System.out.println("Inside checkSpelling." );
    }

}
