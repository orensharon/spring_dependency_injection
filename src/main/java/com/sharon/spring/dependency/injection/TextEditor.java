package com.sharon.spring.dependency.injection;

/**
 * Created by orensharon on 2/27/17.
 */
public class TextEditor {
    private SpellChecker spellChecker;

    // a constructor to inject the dependency.
    public TextEditor(SpellChecker spellChecker) {
        System.out.println("Inside constructor." );
        this.spellChecker = spellChecker;
    }

    public void setSpellChecker(SpellChecker spellChecker) {
        System.out.println("Inside setSpellChecker." );
        this.spellChecker = spellChecker;
    }

    // a getter method to return spellChecker
    public SpellChecker getSpellChecker() {
        return spellChecker;
    }

    public void spellCheck() {
        spellChecker.checkSpelling();
    }
}